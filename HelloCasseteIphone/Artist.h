//
//  Artist.h
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 11/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

// En este archivo esta todo lo que queremos que sea publico para todo el proyecto.

#import <Foundation/Foundation.h>

@interface Artist : NSObject

-(id)initWithName:(NSString*)name
    andYear:(NSNumber*)year
    andBiography:(NSNumber*)biography
    andImage:(NSString*)image
    andIdArtist:(NSNumber*)idArtist
;



// metodo de instancia (hay que crear un objeto)
-(NSString*)JoinStringsWithFirstString:(NSString*)firstString
                       AndSecondString:(NSString*)secondString;

// metodo de clase
+(NSString*)JoinStringsWithFirstString:(NSString*)firstString
                       AndSecondString:(NSString*)secondString;


// creo un metodo de instancia que devuelve el valor de la variale 'name'
-(NSString *)GetName;

@end
