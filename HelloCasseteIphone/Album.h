//
//  Album.h
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject


-(id)initWithName:(NSString*)name
       andIdAlbum:(NSNumber*)idAlbum
          andYear:(NSNumber*)year
         andImage:(NSString*)image
         andCopy:(NSString*)copy
;


@end
