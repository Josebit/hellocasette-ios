//
//  Song.h
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

-(id)initWithName:(NSString*)name
        andIdSong:(NSNumber*)idSong
    andNumberSong:(NSNumber*)numberSong
      andDuration:(NSNumber*)duration
         andAlbum:(NSNumber*)album
       andQuality:(NSNumber*)quality
           andUri:(NSString*)uri
;


@end
