//
//  MusicBO.h
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MusicBO : NSObject


// metodo de instancia (hay que crear un objeto)
-(NSArray*)GetArraysArtists:(NSString*)nameArtist;

-(NSArray*)GetArraysSongs:(NSString*)nameSong;

-(NSArray*)GetArraysAlbums:(NSString*)nameAlbum;




@end
