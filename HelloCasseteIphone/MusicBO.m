//
//  MusicBO.m
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MusicBO.h"
#import "Artist.h"
#import "Song.h"
#import "Album.h"

@implementation MusicBO{
    NSString *_nameArtist;
    NSString *_nameSong;
    NSString *_nameAlbum;
}


-(id)initWithName:(NSString*)name
       andIdAlbum:(NSNumber*)idAlbum
          andYear:(NSNumber*)year
         andImage:(NSString*)image
          andCopy:(NSString*)copy
{
    self = [super init];
    
    if (self){
        _name = name;
        _idAlbum = idAlbum;
        _year = year;
        _image = image;
        _copy = copy;
    }
    
    return self;
}


@end
