//
//  Artist.m
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 11/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Artist.h"

@implementation Artist{
    // declaro variables globales
    NSString *_name;
    NSNumber *_year;
    NSString *_biography;
    NSString *_image;
    NSNumber *_idArtist;
}

// constructor
-(id)initWithName:(NSString *)name
          andYear:(NSNumber *)year
     andBiography:(NSString*)biography
         andImage:(NSString*)image
      andIdArtist:(NSNumber*)idArtist
{
    self = [super init];
    
    if (self){
        _name = name;
        _year = year;
        _biography = biography;
        _image = image;
        _idArtist = idArtist;
    }
    
    return self;
}

-(NSString *)GetName{
    return _name;
}



-(NSString *)JoinStringsWithFirstString:(NSString *)firstString AndSecondString:(NSString *)secondString{
    return [[NSString alloc] initWithFormat:@"%@ %@",firstString,secondString];
}


+(NSString *)JoinStringsWithFirstString:(NSString *)firstString AndSecondString:(NSString *)secondString{
    return [[NSString alloc] initWithFormat:@"%@ %@",firstString,secondString];
}






@end
