//
//  ViewController.m
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 11/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "ViewController.h"
#import "Artist.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    Artist *myArtist = [[Artist alloc] initWithName:@"Extremoduro"
                            andYear:[NSNumber numberWithInt:1985]];
   
    
    NSLog(@"%@", [myArtist GetName]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
