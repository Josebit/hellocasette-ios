//
//  Song.m
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Song.h"

@implementation Song {
    // declaro variables globales
    NSString *_name;
    NSNumber *_idSong;
    NSNumber *_numberSong;
    NSNumber *_duration;
    NSNumber *_album;
    NSNumber *_quality;
    NSString *_uri;
}



-(id)initWithName:(NSString*)name
        andIdSong:(NSNumber*)idSong
    andNumberSong:(NSNumber*)numberSong
      andDuration:(NSNumber*)duration
         andAlbum:(NSNumber*)album
       andQuality:(NSNumber*)quality
           andUri:(NSString*)uri
{
    self = [super init];
    
    if (self){
        _name = name;
        _idSong = idSong;
        _numberSong = numberSong;
        _duration = duration;
        _album = album;
        _quality = quality;
        _uri = uri;
    }
    
    return self;
}



@end
