//
//  Album.m
//  HelloCasseteIphone
//
//  Created by Alberto Rodríguez Carrillo on 12/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Album.h"

@implementation Album{
    // declaro variables globales
    NSString *_name;
    NSNumber *_idAlbum;
    NSNumber *_year;
    NSString *_image;
    NSString *_copy;
}



-(id)initWithName:(NSString*)name
       andIdAlbum:(NSNumber*)idAlbum
          andYear:(NSNumber*)year
         andImage:(NSString*)image
          andCopy:(NSString*)copy
{
    self = [super init];
    
    if (self){
        _name = name;
        _idAlbum = idAlbum;
        _year = year;
        _image = image;
        _copy = copy;
}
    
    return self;
}

@end
